package tw.com.ischool.testjpush;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Set<String> tags = new HashSet<>() ;
        tags.add("kevin");
        tags.add("bill");
        JPushInterface.setTags(MainActivity.this, tags, new TagAliasCallback() {
            @Override
            public void gotResult(int i, String s, Set<String> set) {
//                Log.d("setTags", s);
//                for(String ss : set) {
//                    Log.d("setTags", ss);
//                }
            }
        });
    }
}
