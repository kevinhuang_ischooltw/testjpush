package tw.com.ischool.testjpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by kevinhuang on 2016/6/23.
 */

public class JPushReceiver extends BroadcastReceiver {
    private static String printBundle(Bundle bundle) {
        try {
            StringBuilder sb = new StringBuilder();
            for (String key : bundle.keySet()) {
                sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
            }
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d("TAG", "onReceive - " + intent.getAction() + ", extras: " + printBundle(bundle));


        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            Toast.makeText(context, "ACTION_MESSAGE_RECEIVED", Toast.LENGTH_SHORT).show();
        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            Toast.makeText(context, "ACTION_NOTIFICATION_RECEIVED", Toast.LENGTH_SHORT).show();
        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            Toast.makeText(context, "ACTION_NOTIFICATION_OPENED", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("TAG", "Unhandled intent - " + intent.getAction());
        }
    }
}
