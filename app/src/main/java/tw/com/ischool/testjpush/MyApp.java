package tw.com.ischool.testjpush;

import android.app.Application;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by kevinhuang on 2016/6/23.
 */

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
    }
}
